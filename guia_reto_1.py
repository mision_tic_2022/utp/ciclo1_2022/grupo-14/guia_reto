'''
Ganancias:
si tiempo > 2
valor_interes = (cantidad * porcentaje_interes * tiempo) / 12
porcentaje_interes = 0.03
valor_total = valor_interes + cantidad

------------
Pérdidas:
valor_perder = cantidad * porcentaje_perder
porcentaje_perder = 0.02
valor_total = cantidad - valor_perder

-------------
Parámetros de la función:
    *usuario
    *capital
    *tiempo
    NOTA: La función debe retornar un String

    Retornar para el caso de las ganancias:
    * Para el usuario {} La cantidad de dinero a recibir, según el monto inicial {} para un tiempo de {} meses es: {}

    Retornar para el caso de las pérdidas:
    * Para el usuario {} La cantidad de dinero a recibir, según el monto inicial {} para un tiempo de {} meses es: {}
'''

def CDT(usuario: str, capital: int, tiempo: int):
    mensaje = ''
    if tiempo > 2:
        #Genera ganancias
        porcentaje_interes = 0.03
        valor_interes = (capital * porcentaje_interes * tiempo) / 12
        #Aquí calcular el valor total
        #...
        mensaje = f"Para el usuario {usuario} La cantidad de dinero a recibir, según el monto inicial {} para un tiempo de {tiempo} meses es: {}"
    else:
        #Genera pérdidas
        porcentaje_perder = 0.02
        valor_perder = capital * porcentaje_perder
        #Aquí calcular el valor total
        #...
        mensaje = f"Para el usuario {usuario} La cantidad de dinero a recibir, según el monto inicial {} para un tiempo de {tiempo} meses es: {}"
    
    return mensaje