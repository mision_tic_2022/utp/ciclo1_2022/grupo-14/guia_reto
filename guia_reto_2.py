'''
APUNTES:
*Función debe recibir como parámetro un diccionario ✔️
*Estructura del diccionario a recibir como parámetro:
diccionario = {
    'id_cliente': 0,
    'nombre': '',
    'edad': 0,
    'primer_ingreso': True
}

*La función debe retornar un diccionario con la siguiente estructura: ✔️
respuesta = {
    'nombre': '', 
    'edad': 0, 
    'atraccion': '', 
    'primer_ingreso': True, 
    'total_boleta': 0,
    'apto': True
}

*apto es verdad si está dentro del rango de edad de la tabla
*atraccion y total_boleta -> Si no cumple con el rango de edad se asignarà : 'N/A'

'''

def cliente(informacion: dict)->dict:
    #Crear diccionario respuesta
    respuesta = dict()
    #Obtener edad
    edad = informacion['edad']
    total_boleta = 'N/A'
    atraccion = 'N/A'
    apto = False
    #Evaluar la edad para asignar la atraación y precio
    if  edad > 18:
        total_boleta = 20000
        apto = True
        atraccion = 'X-Treme'
        #Si primer_ingreso es verdad entonces...
        if informacion['primer_ingreso']:
            total_boleta = total_boleta - (total_boleta * 0.05)
    elif edad >= 15:
        pass

    #Asignar información final al diccionario respuesta
    respuesta = {
        'nombre': informacion['nombre'], 
        'edad': informacion['edad'], 
        'atraccion': atraccion,
        'primer_ingreso': informacion['primer_ingreso'],
        'total_boleta': total_boleta,
        'apto': apto
    }

    return respuesta