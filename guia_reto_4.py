

from functools import reduce


rutinaContable_1 = [
    [1201, ("5464", 4, 25842.99), ("7854", 18, 23254.99), ("8521", 9, 48951.95)],
    [1202, ("8756", 3, 115362.58), ("1112", 18, 2354.99)],
    [1203, ("2547", 1, 125698.20), ("2635", 2, 135645.20),("1254", 1, 13645.20), ("9965", 5, 1645.20)],
    [1204, ("9635", 7, 11.99), ("7733", 11, 18.99), ("88112", 5, 390.95)]
]
rutinaContable_2 = [
 [6589, ("9874", 1, 125698.20), ("88112", 2, 135645.20), ("3218", 4, 13645.20)],
 [6590, ("5236", 8, 11.99), ("7733",11,18.99), ("88112", 5, 390.95)],
 [6591, ("7812", 2, 11.99), ("9652",11,18.99)], ]

def ordenes(rutina_contable: list):
    respuesta = '------------------------ Inicio Registro diario ---------------------------------\n'
    #Iterar lista principal
    for lista in rutina_contable:
        #Sacar el id de la lista
        id = lista.pop(0)
        #resp = sum(list(map( lambda tupla: tupla[1]*tupla[2], lista )))
        lista_totales = list(map( lambda tupla: tupla[1]*tupla[2], lista ))
        total = reduce( lambda ac,e: ac+e, lista_totales )
        #Evaluar el total
        if total < 600000:
            total += 25000
        #Concatenar al string de respuesta
        respuesta += f'La factura {id} tiene un total en pesos de {round(total, 2)}\n'
    respuesta += '-------------------------- Fin Registro diario ---------------------------------'

    return respuesta
        

#print(ordenes(rutinaContable_2))

pesos = 520000.33
print('{:,.3f}'.format(pesos) )

#texto=f'La factura {a} tiene un total en pesos de {x:,.2f}'
